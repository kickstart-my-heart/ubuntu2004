.PHONY = environment kickstart packerconf iso clean

environment:
	podman run --rm -it -v ${PWD}:/host:Z --workdir /host registry.gitlab.com/contain-yourself/kickstart-iso-generator:latest

kickstart:
	ansible-playbook playbook.yml --ask-vault-pass

packerconf:
	make clean
	ansible-playbook playbook.yml --ask-vault-pass --extra-vars "ci_environment=local-packer"

iso:
	make clean
	ansible-playbook playbook.yml --ask-vault-pass --extra-vars "ci_environment=local-packer" --extra-vars "repack_iso=repack"

clean:
	find . -maxdepth 1 \( -iname "user_data_*" -o -iname "meta_data*" -o -iname "cloud_init_*.iso" -o -iname "user-data" -o -iname "meta-data" \) -delete 2>/dev/null || true
	rm -rf packer/* 2>/dev/null || true
