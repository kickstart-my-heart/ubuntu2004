---
# Configure VMware Virtual Hardware version here to be used with Packer (optional, default: 13)
##vmware_vhw_ver: "13"

# Configure the VHD size Packer will use here (integer)
##packer_disk_gb: 128

# Configure our install ISO's location
packer_iso_source: 'https://releases.ubuntu.com/20.04.4/ubuntu-20.04.4-live-server-amd64.iso'
packer_iso_sha256: '28ccdb56450e643bad03bb7bcf7507ce3d8d90e8bf09e38f6bd9ac298a98eaad'

# Configure just the ISO name for ISO rebuilds
ubuntu_iso_filename: 'ubuntu-20.04.4-live-server-amd64.iso'
output_iso_filename: 'ubuntu-20.04-4-autoinstall-repack-amd64.iso'

# Use a specific apt mirror (optional)
repo_mirror: 'https://mirror.ox.ac.uk/sites/archive.ubuntu.com/ubuntu/'

# Configure our initial passwords here. It will be hashed for user accounts and GRUB on the fly.
# This will also appear in plaintext in our generated Kickstart CFG file, on the line dealing
# with LUKS encryption for the partition housing the LVM container.
grub_password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      30323965356365366538326166623361353937643862303638336566336630323664333165333036
      6466383835333230303632313563313464373761333933620a373532316632653534613364313530
      35623661656532306661376163363133613932653563303463326265303633633335363935396330
      6637373231626262340a313537646362393834623835363833333530353865366236396538383365
      61653231323734616166383264396364306232393932386431316139356561373337
luks_password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      30323965356365366538326166623361353937643862303638336566336630323664333165333036
      6466383835333230303632313563313464373761333933620a373532316632653534613364313530
      35623661656532306661376163363133613932653563303463326265303633633335363935396330
      6637373231626262340a313537646362393834623835363833333530353865366236396538383365
      61653231323734616166383264396364306232393932386431316139356561373337
user_password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      30323965356365366538326166623361353937643862303638336566336630323664333165333036
      6466383835333230303632313563313464373761333933620a373532316632653534613364313530
      35623661656532306661376163363133613932653563303463326265303633633335363935396330
      6637373231626262340a313537646362393834623835363833333530353865366236396538383365
      61653231323734616166383264396364306232393932386431316139356561373337

# Packer user
packer_user: 'user'
packer_user_password: '{{ user_password }}'

cloud_init_builds:
  # Generic Headless Image
  - name: GenericHeadless
    disk_path: '/dev/sda'
  # Generic Headless Image (CIS-hardened)
  - name: GenericHeadlessCIS
    cis_hardening: true
    disk_path: '/dev/sda'
  # Repacked ISO Image
  - name: RepackedISO
    #apt_proxy: 'http://10.10.10.108:3128/'
    cis_hardening: true
    #disable_auto_upgrades: true
    # We can discover a disk serial number with `udevadm info --query=all /dev/sda | grep ID_SERIAL`
    # We need to use the full ID_SERIAL here, and not the truncated ID_SERIAL_SHORT otherwise curtin just fails to find the disk
    disk_serial: ST3500312CS_6VVHKL9W  # If we specify this (preferred way of doing it), it overrides any disk_path variables we also used
    enable_mta: true
    enable_zfs: true
    hostname: fluffy
    legacy_bios: true
    nas_tools: true
    net_dns_a: '10.10.10.1'
    #net_dns_b: '8.8.8.8'  # Optional
    net_iface: 'eno1'
    net_search: 'home.arpa'
    net_static: true  # We need to set this in order to incorporate any of our other `net_` variables into the netplan config
    net_v4_addr: '10.10.10.11'
    net_v4_cidr: '24'
    net_v4_gateway: '10.10.10.1'

packer_builds:
  # Headless VM (CIS-hardened)
  - name: HeadlessCIS
    cis_hardening: true
    disable_auto_upgrades: true
    disk_path: '/dev/sda'
    enable_mta: true
    enable_zfs: true
    nas_tools: true
    poweroff: true
    remove_machine_id: true
...
