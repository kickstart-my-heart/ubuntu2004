# Ubuntu2004

[![pipeline status](https://gitlab.com/kickstart-my-heart/ubuntu2004/badges/main/pipeline.svg)](https://gitlab.com/kickstart-my-heart/ubuntu2004/-/commits/main) 

Bootable cloud-init ISOs for Ubuntu 20.04 LTS.

This repo is part of my [Kickstart My Heart](https://gitlab.com/kickstart-my-heart) project, where I maintain automated installations for a variety of operating systems.

## Links
* **Main Repo:** https://gitlab.com/kickstart-my-heart/ubuntu2004
* **Compiled Packages:** https://gitlab.com/kickstart-my-heart/ubuntu2004/-/packages

### System requirements
* Download the latest [Ubuntu 20.04 Live Server ISO](https://www.mirrorservice.org/sites/releases.ubuntu.com/focal/).
* Requires a minimum of `64GB` disk space for a `headless` install.
* Requires a minimum of `128GB` disk space for a `workstation` install.

### Configuring build options
To configure build options, copy the default `config.yml`:
```sh
cp -fv files/default/config.yml files/override/config.yml
```
Inside the new `files/override/config.yml` file, you can make changes to the default `kickstart_vars` variable, which defines what Kickstart files will be built when running this playbook. The variable in this file will take priority over the variables in the default `config.yml` file.

This playbook uses [vaulted variables](https://docs.ansible.com/ansible/latest/user_guide/vault.html) to store passwords, mostly as a demonstration of one way this can be done. The vault password for the default passwords is `ansible`. You will be asked for this when running the `make` commands below.

### Compiling cloud-init config files and ISOs locally
Make any necessary changes to the `user-data.j2` or files in the `override/` directory and run the below command to enter the build environment container:

```sh
make
```

### Booting this install (provisioning a 'Golden Master' VM image with Packer)
```sh
make packerconf
```

Now enter the `packer/`subdirectory and choose the relevant area for the virtualisation platform you are using.

And then initialise Packer and build the image (Note that I use the full path to the Packer binary in the Makefile because of [this bug](https://github.com/hashicorp/packer/issues/11081)):
```sh
make build
```

To clear previously-built files from the current directory (this works in both the root directory and in the Packer subdirectories):
```sh
make clean
```

### Booting this install (installing directly to physical hardware, or provisioning a VM without using Packer)
Make just local bootable Kickstart `cidata` ISOs that will autoinstall Ubuntu for us:
```sh
make iso
```

Next steps:
* Attach Ubuntu Live Server ISO to disk bay 1.
* Attach this Kickstart `cidata` ISO to bay 2.
* Boot host.
* Installation should start automatically.

### Post-install steps
```sh
# Change the password for the root account
sudo passwd root

# Change the password for the user account
sudo passwd user
```

### Config Options
* `apt_proxy`
  * String (example: `http://myproxy.corp.net:3128)
  * Default: (unset)
  * If we set this string, it will be used as the proxy setting for apt (for both HTTP and HTTPS sources).
* `cis_hardening`
  * Boolean
  * Default: `false`
  * Controls whether the CIS hardening benchmark will be applied to the target host.
* `disable_auto_upgrades`
  * Boolean
  * Default: `false`
  * Controls whether we override the default Ubuntu behaviour of enabling automatic unattended upgrades (stops the `apt-daily` and `apt-daily-upgrade` systemd timers).
* `disk_encryption`
  * Boolean
  * Default: `false`
  * Defines whether LUKS encryption will be used for the primary disk.
* `enable_mta`
  * Boolean
  * Default: `false`
  * Controls whether to install the `postfix` MTA and `mailutils` utility. Useful for sending emails triggered by automatic upgrades, or by the ZFS Event Daemon.
* `enable_podman`
  * Boolean
  * Default: `false`
  * Controls whether the Podman repository and `podman` package gets installed.
* `enable_snapd`
  * Boolean
  * Default: `false`
  * Controls whether `snapd` gets purged as part of the `late-commands` in the autoinstall.
* `enable_zfs`
  * Boolean
  * Default: `false`
  * Defines whether ZFS will be installed. Uses the upstream binary packages built by Canonical.
* `nas_tools`
  * Boolean
  * Default: `false`
  * If enabled, will install a series of tools I find useful for my own Ubuntu-based NAS deployment, such as `smartmontools` and `sanoid`.
* `poweroff`
  * Boolean
  * Default: `false`
  * Controls whether the `user-data` file instructs the machine to power off at the end of the `cloud-init` run. Useful for Packer installations and generating golden master images.
* `remove_machine_id`
  * Boolean
  * Default: `false`
  * Controls whether `/etc/machine-id` gets removed after the installation is complete. This is mostly useful if creating an image which will be deployed as a template to a large number of servers.
* `vmware_vhw_ver`
  * String
  * Default: `13`
  * Configure the [Virtual Hardware version](https://kb.vmware.com/s/article/1003746) of the VMware VMs that Packer will deploy. A value of `13` provides compatibility with VMware 6.5 and includes support for EFI and Secure Boot. Higher values may provide more recently added features.
* `workstation_deployment`
  * Boolean
  * Default: `false`
  * Controls whether this is a workstation deployment and will use a graphical boot target.

### Changelog
* `20.04.1` - Initial release.
